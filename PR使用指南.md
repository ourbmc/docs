
# 什么是Pull Request

Pull Request译为拉取请求，简称PR，是两个仓库或同仓库内不同分支之间提交变更的一种途径，同时也是一种非常好的团队协作方式，常用于团队的代码审查等场景。你拉取并修改了他人的代码后，将你的修改告知给代码的原作者，请求他合并你的修改，就称为Pull Request。

# Pull Request执行流程

当你想修改别人仓库里的代码时，会执行以下流程：

1\. 首先fork他人的仓库，拷贝出一份和原仓库相同的仓库来。

2\. 将你要修改的分支clone到本地分支，进行修改。

3\. 发起Pull Request给原仓库，这样原作者就可以看见你做的修改并进行审核。如果他认可了你的修改，就会将你的分支merge到他的项目中。

# 创建Pull Request

参与Gitee中的仓库开发，最常用方式是“Fork+Pull”模式。在“Fork+Pull”模式下，仓库参与者不必向仓库创建者申请提交权限，而是在自己的托管空间下建立仓库的派生（Fork）。至于在派生仓库中创建的提交，可以非常方便地利用Gitee的Pull Request工具向原始仓库的维护者发送Pull Request。

（1）Fork仓库

fork仓库是非常简单的，进到仓库页面，然后找到右上角的fork按钮，点击后选择fork到的命名空间，再点击确认，等待系统在后台完成仓库克隆操作，就完成了fork操作，然后就可以在本地对仓库代码进行修改。
![输入图片说明](img/Fork.png)


（2）提交PR

![输入图片说明](img/%E6%8F%90%E4%BA%A4PR.png)

首先，您的仓库与目标仓库必须存在差异，这样才能提交，比如这样：

![输入图片说明](img/%E4%B8%8E%E4%BB%93%E5%BA%93%E6%9C%89%E5%B7%AE%E5%BC%82.png)

如果不存在差异，或者目标分支比你提Pull Request的分支还要新，则会得到这样的提示：

![输入图片说明](img/%E4%B8%8E%E4%BB%93%E5%BA%93%E6%97%A0%E5%B7%AE%E5%BC%82.png)

然后，填入Pull Request的说明，点击提交Pull Request，就可以提交一个Pull Request了，就像下图所示的那样：

![输入图片说明](img/PR%E6%8F%90%E4%BA%A4%E6%88%90%E5%8A%9F.png)

（3）提交PR内容填写

在提交PR页面左侧填入标题和相关的变更内容说明，右侧是几个配置项。下面简单介绍这些配置项的用法。

-   审查人员：用于审查该次提交的相关人员，一般是仓库的负责人。
-   测试人员：用于测试该次提交的相关人员，一般是仓库的负责人。
-   优先级：提交人可以给PR设置优先级。
-   标签：提交人可以给PR设置标签。
-   关联Issue：可以给该PR关联Issue。用户可以在关闭Pull Request的时候同时关闭issue。
-   里程碑：相关的PR可以放到一个里程碑里。里程碑通常可以理解为不同的版本或不同的迭代。

# Pull Request草稿功能

Gitee现已支持提交PR草稿的功能，当项目成员还没有完成开发时，可以在提交PR时选择创建Pull Request草稿。

![输入图片说明](img/PR%E8%8D%89%E7%A8%BF.png)

同时，使用PR草稿的功能也有助于让其他成员检查你的Fork以获得反馈，以草稿形式提交的PR会在该PR的各个相关页面给予提示，并且该PR无法合并，当准备好进行代码审查时，可以取消PR的草稿状态，进行正常的代码审查与合并。

![输入图片说明](img/%E8%8D%89%E7%A8%BF%E8%AF%A6%E6%83%85.png)

# Pull Request关联Issue

通过Pull Request关联Issue，用户可以在关闭Pull Request的时候同时关闭issue。关联功能具有以下特点：

-   一个PR可以关联多个issue，例如同时关联issue1，issue2格式为：\#issue1ident, \#issue2dent
-   PR关联issue后，issue的状态会自动更改为进行中，当PR被合并后，issue会更改为关闭状态。

个人版和企业版的区别：

-   个人版，PR只能关联当前仓库的任务
-   企业版，PR可以关联所有企业的任务。

# 对Pull Request的bug修改如何提交到该Pull Request中

对于Pull Request中的bug修复或者任何更新动作，均不必要提交新的Pull Request，仅仅只需要推送到您提交Pull Request的分支上，后台会自动更新这些提交，将其加入到这个Pull Request中去。
